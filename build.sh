#! /usr/bin/env bash
set -e

MACHINE=`uname -m`_`uname -s`
CC=`which gcc`
CXX=`which g++`
export CC=$CC
export CXX=$CXX

mkdir -p build/$MACHINE
mkdir -p temp/$MACHINE

cd temp/$MACHINE

../../python/configure --enable-optimizations --prefix=`pwd`/../../build/$MACHINE/
make -j4
make install